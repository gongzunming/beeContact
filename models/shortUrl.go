package models

import (
	"github.com/astaxie/beego/orm"
)

type ShortUrl struct {
	Id          int64 `orm:"pk;column(id);"`
	Url         string
	Short_url   string
	Add_time    string
	Expire_time string
}

func (this *ShortUrl) TableName() string {
	return TableName("short_url")
}

func init() {
	orm.RegisterModel(new(ShortUrl))
}
func AddShortUrl(url *ShortUrl) error {
	o := orm.NewOrm()
	shorturl := new(ShortUrl)
	shorturl.Url = url.Url
	shorturl.Short_url = url.Short_url
	shorturl.Add_time = "2017-01-08 8:29:04"
	shorturl.Expire_time = "2017-02-08 8:29:04"
	_, err := o.Insert(shorturl)
	return err
}
