package controllers

import (
	"beeContact/models"

	"github.com/astaxie/beego"
)

type DefaultController struct {
	beego.Controller
}

func (c *DefaultController) Get() {
	c.Data["WebTitle"] = "阳江一中高三(11)通讯录"
	c.TplName = "index.html"
}

func (c *DefaultController) Post() {
	short_url := &models.ShortUrl{}

	short_url.Url = c.Ctx.Input.Param("url")

	c.Data["id"] = 12
	c.ServeJSON()
}

func (this *DefaultController) Generate() {
	url := this.GetString("url") //this.Ctx.Input.Param("url")
	short_url := &models.ShortUrl{}
	short_url.Url = this.GetString("url")
	short_url.Short_url = "wqeq"
	models.AddShortUrl(short_url)
	this.Data["id"] = url
	this.TplName = "generate.html"
}
