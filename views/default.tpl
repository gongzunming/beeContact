<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">

  <title>{{.WebTitle}}</title>
  <!-- 新 Bootstrap 核心 CSS 文件 -->
  <link rel="stylesheet" href="//cdn.bootcss.com/bootstrap/3.3.5/css/bootstrap.min.css">

  <!-- 可选的Bootstrap主题文件（一般不用引入） -->
  <link rel="stylesheet" href="//cdn.bootcss.com/bootstrap/3.3.5/css/bootstrap-theme.min.css">
</head>
<body>
  <section class="container">
    <h4 class="text-center">{{.WebTitle}}</h4>
    <div class="col-xs-12">
      <form action="/default" method="post">
        <div class="form-group">
          <label for="wechat">姓名</label>
          <input type="text" class="form-control" id="wechat" name="wechat" placeholder="Wechat">
        </div>
        <div class="form-group">
          <label for="phone">常用手机号码</label>
          <input type="text" class="form-control" id="phone" name="phone" placeholder="常用手机号码">
        </div>
        <div class="form-group">
          <label for="exampleInputEmail1">邮箱</label>
          <input type="email" class="form-control" id="exampleInputEmail1" name="exampleInputEmail1" placeholder="Email">
        </div>
        <div class="form-group">
          <label for="wechat">微信</label>
          <input type="text" class="form-control" id="wechat" name="wechat" placeholder="Wechat">
        </div>
        <div class="form-group">
          <label for="workPlace">工作地点</label>
          <input type="text" class="form-control" id="workPlace" name="workPlace" placeholder="工作地点">
        </div>
        <div class="form-group">         
            <button type="submit" class="btn btn-default center-block">提交</button>        
        </div>
      </form>
    </div>
    
  </section>
</body>
</html>