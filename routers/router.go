package routers

import (
	"beeContact/controllers"

	"github.com/astaxie/beego"
)

func init() {
	beego.Router("/", &controllers.DefaultController{})
	beego.Router("/default", &controllers.DefaultController{})
	beego.Router("/generate", &controllers.DefaultController{}, "*:Generate")
}
