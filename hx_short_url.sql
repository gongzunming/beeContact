/*
 Navicat MySQL Data Transfer

 Source Server         : localhost
 Source Server Version : 50717
 Source Host           : localhost
 Source Database       : aiopms

 Target Server Version : 50717
 File Encoding         : utf-8

 Date: 04/04/2017 22:12:04 PM
*/

SET NAMES utf8;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
--  Table structure for `hx_short_url`
-- ----------------------------
DROP TABLE IF EXISTS `hx_short_url`;
CREATE TABLE `hx_short_url` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `url` varchar(500) NOT NULL COMMENT '原网址',
  `short_url` varchar(100) NOT NULL COMMENT '短网址',
  `add_time` datetime NOT NULL COMMENT '生成时间',
  `expire_time` datetime NOT NULL COMMENT '过期时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='短网址';

SET FOREIGN_KEY_CHECKS = 1;
